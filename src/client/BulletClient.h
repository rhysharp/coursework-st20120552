#pragma once
#ifndef BULLET_CLIENT_H
#define BULLET_CLIENT_H

#include "Bullet.h"
#include "spriteComponent.h"

class BulletClient : public Bullet
{
public:
	static	GameObjectPtr	StaticCreate() { return GameObjectPtr(new BulletClient()); }

	virtual void		Read(InputMemoryBitStream& inInputStream) override;
	virtual bool		HandleCollisionWithPlayer(Player* inPlayer) override;

protected:
	BulletClient();

private:

	SpriteComponentPtr	mSpriteComponent;
};
#endif //BULLET_CLIENT_H