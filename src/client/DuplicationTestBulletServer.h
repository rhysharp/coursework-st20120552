#pragma once
#ifndef BULLET_CLIENT_H
#define BULLET_CLIENT_H

#include "Bullet.h"
#include "NetworkManagerServer.h"

class BulletServer : public Bullet
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new BulletServer()); }
	void HandleDying() override;

	virtual bool		HandleCollisionWithPlayer(Player* inPlayer) override;

	virtual void Update() override;

protected:
	BulletServer();

private:
	float mTimeToDie;

};
#endif //BULLET_CLIENT_H