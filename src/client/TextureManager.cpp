

#include "TextureManager.h"
#include "GraphicsDriver.h"
#include <SDL_image.h>

#include "StringUtils.h"

std::unique_ptr< TextureManager >		TextureManager::sInstance;

/// <summary>
///	This instantiates the TextureManager Client Side
/// </summary>
void TextureManager::StaticInit()
{
	sInstance.reset( new TextureManager() );
}

/// <summary>
///	This caches the textures needed, allowing easy access
/// </summary>
TextureManager::TextureManager()
{
	/* This should probably load from a config file or something */
	CacheTexture( "player", "../assets/player.png" );
	CacheTexture( "bullet", "../assets/bullet.png" );

}

/// <summary>
///	This gets a certain texture depending on the name provided. 
/// </summary>
TexturePtr	TextureManager::GetTexture( const string& inTextureName )
{
	return mNameToTextureMap[ inTextureName ];
}

/// <summary>
///	This stores the textures requested, if a texture files name cannot be found, an error will be given.
/// </summary>
bool TextureManager::CacheTexture( string inTextureName, const char* inFileName )
{
	SDL_Texture* texture = IMG_LoadTexture( GraphicsDriver::sInstance->GetRenderer(), inFileName );

	if( texture == nullptr )
	{
		SDL_LogError( SDL_LOG_CATEGORY_ERROR, "Failed to load texture: %s", inFileName );
		LOG("Failed to load texture: %s", inFileName);
		return false;
	}

	int w, h;
	SDL_QueryTexture( texture, nullptr, nullptr, &w, &h );

	// Set the blend mode up so we can apply our colors
	SDL_SetTextureBlendMode( texture, SDL_BLENDMODE_BLEND );

	TexturePtr newTexture( new Texture( w, h, texture ) );

	mNameToTextureMap[ inTextureName ] = newTexture;

	return true;

}


