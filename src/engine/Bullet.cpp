#include <Player.h>
#include <Bullet.h>
#include "Maths.h"

//maybe
#include "Timing.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

#include <Windows.h>
#include <Mmsystem.h>
#include <mciapi.h>

/// <summary>
///	This sets up the values for bullet to use 
/// </summary>
Bullet::Bullet() :
	GameObject(),
	mMuzzleSpeed(4.f),
	mVelocity(Vector3::Zero),
	mPlayerId(0)
{
	SetScale(GetScale() * 0.25f);
	SetCollisionRadius(0.125f);
}


uint32_t Bullet::Write(OutputMemoryBitStream& inOutputStream, uint32_t inDirtyState) const
{
	uint32_t writtenState = 0;

	if (inDirtyState & EYRS_Pose)
	{
		inOutputStream.Write((bool)true);

		Vector3 location = GetLocation();
		inOutputStream.Write(location.mX);
		inOutputStream.Write(location.mY);

		Vector3 velocity = GetVelocity();
		inOutputStream.Write(velocity.mX);
		inOutputStream.Write(velocity.mY);

		inOutputStream.Write(GetRotation());

		writtenState |= EYRS_Pose;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	if (inDirtyState & EYRS_Color)
	{
		inOutputStream.Write((bool)true);

		inOutputStream.Write(GetColor());

		writtenState |= EYRS_Color;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}

	if (inDirtyState & EYRS_PlayerId)
	{
		inOutputStream.Write((bool)true);

		inOutputStream.Write(mPlayerId, 8);

		writtenState |= EYRS_PlayerId;
	}
	else
	{
		inOutputStream.Write((bool)false);
	}


	return writtenState;
}

/// <summary>
///	This handles the collison with the player
/// </summary>
bool Bullet::HandleCollisionWithPlayer(Player* inPlayer)
{
	(void)inPlayer;

	//you hit a tank, so look like you hit a tank
	return false;
}

/// <summary>
///	This instantiates the bullet from the shooter, taking atributres from that specific player, e.g Location to spawn and direction.
/// </summary>
void Bullet::InitFromShooter(Player* inShooter)
{
	SetColor(inShooter->GetColor());
	SetPlayerId(inShooter->GetPlayerId());

	Vector3 forward = inShooter->GetForwardVector();
	SetVelocity(inShooter->GetVelocity() + forward * mMuzzleSpeed);
	SetLocation(inShooter->GetLocation() + forward * 1.0f );

	SetRotation(inShooter->GetRotation());

	//PlaySound(TEXT("Shoot.wav"), NULL, SND_SYNC);

	
}

/// <summary>
///	This updates the bullets movement overtime
/// </summary>
void Bullet::Update()
{

	float deltaTime = Timing::sInstance.GetDeltaTime();

	SetLocation(GetLocation() + mVelocity * deltaTime);


	//we'll let the Player handle the collisions
}