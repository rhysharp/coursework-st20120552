#include "Timing.h"
#include "StringUtils.h"

#include "NetworkManager.h"

float kDesiredFrameTime = 0.0166f;
#if !_WIN32
	#include <chrono>
	using namespace std::chrono;
#endif

Timing	Timing::sInstance;

namespace
{
#if _WIN32
#include <windows.h>
	LARGE_INTEGER sStartTime = { 0 };
#else
	high_resolution_clock::time_point sStartTime;
#endif
}


Timing::Timing()
{
#if _WIN32
	LARGE_INTEGER perfFreq;
	QueryPerformanceFrequency( &perfFreq );
	mPerfCountDuration = 1.0 / perfFreq.QuadPart;

	QueryPerformanceCounter( &sStartTime );

	mLastFrameStartTime = GetTime();
#else
	sStartTime = high_resolution_clock::now();
#endif
}

void Timing::Update()
{
	ticksPassed++;

	double currentTime = GetTime();

    mDeltaTime = ( float ) ( currentTime - mLastFrameStartTime );
	
	//Output
	//LOG("Results: ", currentTime);

	//LOG("Results: ", );
	

	////frame lock at 60fps
	//while( mDeltaTime < kDesiredFrameTime )
	//{
	//	currentTime = GetTime();

	//	mDeltaTime = (float)( currentTime - mLastFrameStartTime );
	//}

	mLastFrameStartTime = currentTime;
	mFrameStartTimef = static_cast< float > ( mLastFrameStartTime );

	//if TIMER IS ACTIVE, CHECK IF 20 SECONDS HAS PASSED
	if (timerActive && mLastFrameStartTime - timerStartTime > 20.0)
	{	//if current time - timer start is greater than 20
		//Stop timer
		timerActive = false;

		//Commented out for DroppedPacketChange Test
		//output = ticksPassed / (mLastFrameStartTime - timerStartTime);

		LOG("Results are %f", output);
	}
}

	///Function called in InputManager.cpp to start the timer.
	void Timing::StartTimer() 
	{
		if (timerActive) return;
		//Stops continuation if timer has already started. 
		timerActive = true;

		//Set timer to current time. 
		timerStartTime = mLastFrameStartTime;

		ticksPassed = 0;

		LOG("Timer Started", 0);

	}	
	
	void Timing::WhenTimerEnds(float results)		//Outputs whatever when timer finishes. 
	{
		//LOG("Timer Ended ", 0);
		output = results;
		//LOG("Byte Count Results : %f", results);
	}


double Timing::GetTime() const
{
#if _WIN32
	LARGE_INTEGER curTime, timeSinceStart;
	QueryPerformanceCounter( &curTime );

	timeSinceStart.QuadPart = curTime.QuadPart - sStartTime.QuadPart;

	return timeSinceStart.QuadPart * mPerfCountDuration;
#else
	auto now = high_resolution_clock::now();
	auto ms = duration_cast< milliseconds >( now - sStartTime ).count();
	//a little uncool to then convert into a double just to go back, but oh well.
	return static_cast< double >( ms ) / 1000;
#endif
}
