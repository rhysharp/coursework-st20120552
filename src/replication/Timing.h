#ifndef TIMING_H_
#define TIMING_H_

//For uint64_t
#include <cstdint>

class Timing
{
public:

	Timing();

	void Update();

	float GetDeltaTime() const { return mDeltaTime; }

	double GetTime() const;

	float GetTimef() const
	{
		return static_cast< float >( GetTime() );
	}

	float GetFrameStartTime() const { return mFrameStartTimef; }

	void StartTimer();
	void WhenTimerEnds(float);

	static Timing sInstance;

	//Testing purposes
	bool		timerActive = false;

private:
	float		mDeltaTime;
	uint64_t	mDeltaTick;

	double		mLastFrameStartTime;
	float		mFrameStartTimef;
	double		mPerfCountDuration;

	//Testing purposes.
	float		output;
	int			ticksPassed;
	float		timerStartTime;

};

#endif  // TIMING_H_
