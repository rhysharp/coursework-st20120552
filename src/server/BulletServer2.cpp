#include <BulletServer2.h>
#include "PlayerServer.h"

#include "ClientProxy.h"
#include "Timing.h"
#include "MoveList.h"
#include "Maths.h"

/// <summary>
///	This sets the bullets time to die from the startfromtime
/// </summary>
BulletServer2::BulletServer2()
{
	//Bullet lives 4 second...
	mTimeToDie = Timing::sInstance.GetFrameStartTime() + 4.f;
}

/// <summary>
///	This handles the removal of the bullet object by unregistering it
/// </summary>
void BulletServer2::HandleDying()
{
	NetworkManagerServer::sInstance->UnregisterGameObject(this);
}

/// <summary>
///	This updates the bulletServer, and runs update in bullet and keeps track of when the bullet should die. 
/// </summary>
void BulletServer2::Update()
{
	Bullet::Update();

	if (Timing::sInstance.GetFrameStartTime() > mTimeToDie)
	{
		SetDoesWantToDie(true);
	}

}

/// <summary>
///	This handles the bullets collision with the player server side, setting the bullets bool value to die to true and telling playerserver to take damage. 
/// </summary>
bool BulletServer2::HandleCollisionWithPlayer(Player* inPlayer)
{
	if (inPlayer->GetPlayerId() != GetPlayerId())
	{
		//kill yourself!
		SetDoesWantToDie(true);

		static_cast<PlayerServer*>(inPlayer)->TakeDamage(GetPlayerId());

	}

	return false;
}
