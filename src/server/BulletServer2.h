#pragma once
#ifndef BULLET_SERVER2_H
#define BULLET_SERVER2_H

#include "Bullet.h"
#include "NetworkManagerServer.h"

class BulletServer2 : public Bullet
{
public:
	static GameObjectPtr	StaticCreate() { return NetworkManagerServer::sInstance->RegisterAndReturn(new BulletServer2()); }
	void HandleDying() override;

	virtual bool		HandleCollisionWithPlayer(Player* inPlayer) override;

	virtual void Update() override;

protected:
	BulletServer2();

private:
	float mTimeToDie;

};
#endif //BULLET_SERVER2_H